import numpy as np


class C2DLine(object):
    def __init__(self,P1,P2,fromvector=False):
        if fromvector:
            self.v = P1
            self.p = P2
        else:
            self.p=P1
            self.v=P2-P1

    def IntersectVector(self, b):
        dim_problem=np.shape(self.v)

        a=np.zeros((dim_problem[0]*2,dim_problem[0]*2),dtype=float)
        arrays = np.array([np.array([v, b]).transpose() for v,b in zip(self.v,b.v)])

        dim_r,dim_c=np.shape(a)
        indexes=[[i*2,(i+1)*2] for i in range(dim_problem[0])]

        for j,i  in enumerate(indexes):
            a[i[0]:i[1],i[0]:i[1]]=arrays[j]
        c=b.p-self.p

        dim_c=np.shape(c)
        c=np.reshape(c,(dim_c[0]*dim_c[1],),order='C')
        #print(c)
        X=np.linalg.solve(a,c)
        #print("array:\n",a)
        ##print("const:\n",c)
        ##print("sol:\n",X)
        K_r=[v for i, v in enumerate(X) if i % 2 == 0]
        ##print("K_r:\n",K_r)
        ##print("self.p:\n",self.p)
        ##print("self.v:\n", self.v)
        point = [p + k*( v) for p,k,v in zip(self.p,K_r,self.v)]
        #print("point:\n",point)
        return point

    def Intersect(self,b):
        array=np.array([self.v,b.v]).transpose()
        c=b.p-self.p
        inv_array=np.linalg.inv(array)
        k_=np.linalg.solve(array,c)
        point=self.p+k_[0]*self.v
        #print("array:\n",array)
        #print("const:\n",c)
        #print("sol:\n",k_)
        #print("self.p:\n", self.p)
        #print("self.v:\n", self.v)
        #print("point:\n",point)
        return point

class C2DVanishingPlane(object):
    def __init__(self,A,B,C,D):
        self.vp1 = C2DLine(A,B).Intersect(C2DLine(D,C))
        self.vp2 = C2DLine(A,D).Intersect(C2DLine(B,C))
        self.SetMapping(A,B,C,D)

    def Transform(self,p,vanishingpoint=0):
        if vanishingpoint==0:
            lambda_=np.linalg.norm(self.vp1-p)
        else:
            lambda_ = np.linalg.norm(self.vp2 - p)
        return lambda_
    def SetMapping(self,A,B,C,D):
        self.lambda_A= self.Transform(A)
        self.lambda_B =self.Transform(B)
        self.lambda_C = self.Transform(C)
        self.lambda_D = self.Transform(D)
        self.v11 = A-self.vp1
        self.v11 = self.v11/np.linalg.norm(self.v11)
        self.v12 = D - self.vp1
        self.v12 = self.v12 / np.linalg.norm(self.v12)
        self.v21 = A - self.vp2
        self.v21 = self.v21 / np.linalg.norm(self.v21)
        self.v22 = B - self.vp2
        self.v22 = self.v22 / np.linalg.norm(self.v22)




    def Mapping(self,k,lambda_):
        lambda_k=(self.lambda_A*(1-k)+self.lambda_D*k)*(1-lambda_)+(self.lambda_B*(1-k)+self.lambda_C*k)*lambda_
        v1_k=(1-k)*self.v11+k*self.v12
        M=self.vp1+lambda_k*v1_k
        return M
    def MappingVectorize(self,alpha,beta):
        num_alpha=np.shape(alpha)[0]
        num_beta=np.shape(beta)[0]
        stack_alpha=np.tile(np.column_stack([alpha,alpha]),num_beta).reshape((num_alpha*num_beta,2))
        v1=(1-stack_alpha)*self.v11+stack_alpha*self.v12

        x = np.arange(0, 1, 1/num_alpha)
        y = np.arange(0, 1, 1/num_beta)
        a,b= np.meshgrid(x, y, sparse=True)
        #a=a.transpose()
      #  z = np.sin(xx ** 2 + yy ** 2) / (xx ** 2 + yy ** 2)
        #h = plt.contourf(x, y, z)
        lmbda =(self.lambda_A*(1-a)+self.lambda_D*a)*b+(self.lambda_B*(1-a)+self.lambda_C*a)*(1-b)
        # ok. Lo tengo que hacer a base de mesh grids, pensando por el final
        # es decir, M tiene que ser una matriz de alpha x beta x 2
        # para cada M[alpha,beta] =

        return
    def MappingVector(self,k_,lambda__):
        lambda_k = np.array([[[(self.lambda_A * (1 - k) + self.lambda_D * k) * (1 - lambda_) + (
                    self.lambda_B * (1 - k) + self.lambda_C * k) * lambda_] for lambda_ in lambda__] for k in k_ ])

        v1_k =np.array([[(1 - k) * self.v11 + k * self.v12]for k in k_])

        dim_k=len(k_)
        dim_lambda=len(lambda__)
        M=np.array([[[self.vp1+lambda_k[i][j]*v1_k[i]] for j in range(0,dim_lambda)] for i in range(0,dim_k)]).reshape(dim_k,dim_lambda,2).round(0)
        M=np.array(M,dtype=int)
        #print(M)
        #print(np.shape(M))
        return M


def Unit_test():
    # A = np.array([1206, 1349])
    # B = np.array([3754, 1348])
    # C = np.array([4021, 2530])
    # D = np.array([939, 2531])

    A = np.array([2, 4])
    B = np.array([5, 3])
    C = np.array([7, 4])
    D = np.array([0, 10])

    # A =np.array([0, 0])
    # B =np.array([3, 0])
    # C =np.array([4, 3])
    # D =np.array([-1, 2])

    a = np.array([0,0])
    b = np.array([2, 0])
    c = np.array([2, 2])
    d = np.array([0, 2])

    width=int(round(np.linalg.norm(b-a)))
    heigth=int(round(np.linalg.norm(d-a)))

    A_B=(A+B)/2
    B_C=(C+B)/2
    C_D=(C+D)/2
    D_A=(D+A)/2

    planev=C2DVanishingPlane(A,B,C,D)

    point_A=planev.Mapping(0,0)
    point_B=planev.Mapping(0,1)
    point_D=planev.Mapping(1,0)
    point_C=planev.Mapping(1,1)

    k=np.array([elem/width for elem in np.arange(width+1)])
    print("k=:",k)
    lambda_=np.array([elem/heigth for elem in np.arange(heigth+1)])
    print("lambda=:",lambda_)
    points=planev.MappingVector(k,lambda_)
    points2=planev.MappingVectorize(k,lambda_)
    dim=np.shape(points)

    M=points

    print("M[0,0]=",M[0,0],"pl(0,0)=",planev.Mapping(0,0))
    print("M[0,2]=",M[0, 2],"pl(0,2)=",planev.Mapping(0,2/heigth))
    print("M[2,2]=",M[2, 2],"pl(2,2)=",planev.Mapping(2/width,2/heigth))
    print("M[2,2]=",M[2, 0],"pl(2,0)=",planev.Mapping(2/width,0/heigth))

    array=np.random.randint(0,255,(11,11,3))
    # print("random RGB array:")
    # print(array)
    # array_t=np.zeros((11,11,3),dtype=int)
    print("Pixel[0,0]=",array[M[0,0][0],M[0,0][1]])
    # print("Pixel[0,2]=", array[M[0, 2]])
    # print("Pixel[2,2]=", array[M[2, 2]])
    # print("Pixel[2,0]=", array[M[2, 0]])
#Unit_test()


