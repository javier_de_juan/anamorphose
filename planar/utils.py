import math
import numpy as np

def progbar2(curr,total, full_progbar,text):
    frac = curr / total
    filled_progbar = round(frac * full_progbar)
    print('\r',text,'#' * filled_progbar + '-' * (full_progbar - filled_progbar), '[{:>7.2%}]'.format(frac), end='')


def progbar(curr, total, full_progbar):
    frac = curr/total
    filled_progbar = round(frac*full_progbar)
    print('\r', '#'*filled_progbar + '-'*(full_progbar-filled_progbar), '[{:>7.2%}]'.format(frac), end='')



def hh_mm_ss(segundos):
    horas = int(segundos/3600)
    segundos_restantes = segundos % 3600
    minutos = int(segundos_restantes / 60)
    segundos_finales = int(segundos_restantes % 60)
    return (horas, minutos, segundos_finales)

def hh_mm_ss_text(segundos):
    horas = int(segundos/3600)
    segundos_restantes = segundos % 3600
    minutos = int(segundos_restantes / 60)
    segundos_finales = int(segundos_restantes % 60)
    return "%d hr %d mn %d s"%(horas, minutos, segundos_finales)


def divisorGenerator(n):
    large_divisors = []
    for i in range(1, int(math.sqrt(n) + 1)):
        if n % i == 0:
            yield i
            if i*i != n:
                large_divisors.append(n / i)
    for divisor in reversed( large_divisors):
        yield divisor

