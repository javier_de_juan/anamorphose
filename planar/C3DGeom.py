import numpy as np
#import pygame
from definitions import *
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from quaternion import *
from dataclasses import  dataclass


@dataclass
class C3dObject():

    def __init__(self,scene,name,father,color,origin,axis_x=np.array([1,0,0]),axis_y=np.array([0,1,0]),axis_z=np.array([0,0,1])):


        self.name=name
        if scene :
            self.scene=scene
            self.screen=scene.screen
        self.origin=np.array(origin)
        self.axis_x=np.array(axis_x)
        self.axis_y=np.array(axis_y)
        self.axis_z=np.array(axis_z)
        #self.axis_z=np.cross(axis_x,axis_y)
        
        
        self.father=father
        self.matrix=np.array([axis_x,axis_y,axis_z],dtype=float)
        self.inv_matrix = np.linalg.inv(self.matrix)
        self.color = color
        self.factorx=1.0
        self.factory=1.0
        self.factorz=1.0
        self.isactivereference=False
        self.activeaxis=0
        self.speed=np.array([0,0,0])
        self.accel=np.array([0,0,0])
        self.cinematics=None
        self.render=False
        self.skip=False
        self.skipRender=False

       
      
       
        
    def Render(self,camera):

        if not self.isactivereference or self.skipRender:
           return

       
        self.color = YELLOW
        #vector x
        width=2 if self.activeaxis == 0 else 1
        p1=self.origin
        p1_=self.ModeltoWorld(p1,camera)
        p2=self.origin+self.axis_x*20
        p2_=self.ModeltoWorld(p2,camera)
        

        pygame.draw.line(self.screen,self.color,p1_,p2_,width)
        
        #flechitas vector x
        p3=p2+np.array([-1,1,0]).dot(self.matrix)
        p3_=self.ModeltoWorld(p3,camera)

        pygame.draw.line(self.screen,self.color,p2_,p3_,width)

        p3=p2+np.array([-1,-1,0]).dot(self.matrix)
        p3_=self.ModeltoWorld(p3,camera)

        pygame.draw.line(self.screen,self.color,p2_,p3_,width)

        self.color = RED
        #vector y
        width=2 if self.activeaxis == 1 else 1
        
        p2=self.origin+self.axis_y*20
        p2_=self.ModeltoWorld(p2,camera)

        pygame.draw.line(self.screen,self.color,p1_,p2_,width)

        #flechitas vector y
        p3=p2+np.array([1,-1,0]).dot(self.matrix)
        p3_=self.ModeltoWorld(p3,camera)

        pygame.draw.line(self.screen,self.color,p2_,p3_,width)

        p3=p2+np.array([-1,-1,0]).dot(self.matrix)
        p3_=self.ModeltoWorld(p3,camera)

        pygame.draw.line(self.screen,self.color,p2_,p3_,width)

        self.color = GREEN
        #vector z
        width=2 if self.activeaxis == 2 else 1
        p1=self.origin
        p1_=self.ModeltoWorld(p1,camera)
        p2=self.origin+self.axis_z*20
        p2_=self.ModeltoWorld(p2,camera)
  
        pygame.draw.line(self.screen,self.color,p1_,p2_,width)

        #flechitas vector z
        p3=p2+np.array([1,0,-1]).dot(self.matrix)
        p3_=self.ModeltoWorld(p3,camera)

        pygame.draw.line(self.screen,self.color,p2_,p3_,width)

        p3=p2+np.array([-1,0,-1]).dot(self.matrix)
        p3_=self.ModeltoWorld(p3,camera)

        pygame.draw.line(self.screen,self.color,p2_,p3_,width)



      

    
    def Update(self,delta):

        if self.cinematics!=None:
            
            self.cinematics.Update(delta)

            #self.accel=-self.origin/10
            #self.speed=self.speed+self.accel*delta/1000
            #self.origin=self.origin+self.speed*delta/1000

    def Scale(self,factor):

        self.factorx=self.factory=self.factorz=factor
        factor_matrix=np.array([[self.factorx,0,0],[0,self.factory,0],[0,0,self.factorz]])
        self.matrix=factor_matrix.dot(self.matrix)
        self.axis_x=self.matrix[0,]
        self.axis_y=self.matrix[1,]
        self.axis_z=self.matrix[2,]

    def ScaleAxis(self,factor):

        self.factorx=factor if self.activeaxis == 0 else 1
        self.factory=factor if self.activeaxis == 1 else 1
        self.factorz=factor if self.activeaxis == 2 else 1
        
    def Transform(self):

        factor_matrix=np.array([[self.factorx,0,0],[0,self.factory,0],[0,0,self.factorz]])
        self.matrix=factor_matrix.dot(self.matrix)
        self.axis_x=self.matrix[0,]
        self.axis_y=self.matrix[1,]
        self.axis_z=self.matrix[2,]

    def WorldtoScreen(self,point,camera):
        
        wx=point[0]
        wy=point[1]
        wz=point[2]
        cx=camera.origin[0]
        cy=camera.origin[1]
        cz=camera.origin[2]

        x=(wx-cx)*cz/(cz-wz)+cx
        y=(wy-cy)*cz/(cz-wz)+cy

        return np.array([x,y])

    def ModeltoWorldMatrix(self,m):
      
        if self.father!=None:
            m=m.dot(self.father.matrix)
            return self.father.ModeltoWorldMatrix(m)
        else:
            return m

    def ModeltoWorldRe(self,point):

        retpoint=point
        if self.father!=None:
            retpoint=point.dot(self.father.matrix)+self.father.origin
            return self.father.ModeltoWorldRe(retpoint)
        else:
            return retpoint

    def ModeltoWorld(self,point,camera):

        point=self.ModeltoWorldRe(point)
        return self.WorldtoScreen(point,camera)

    def WorldToModel(self,point):
        return (point-self.origin).dot(self.inv_matrix)

    def OrthoNormalMatrix(self):
        return np.array([self.axis_x/np.linalg.norm(self.axis_x),
                         self.axis_y/np.linalg.norm(self.axis_y),
                         self.axis_z/np.linalg.norm(self.axis_z)])
     
    def Rotate(self,axis,angle):

        imatrix=self.OrthoNormalMatrix()
  
        axis=imatrix[axis,]

        qaxis = CQuat()
        qmatrix=CQuat()

        qaxis.fromAxisAngle(axis,angle)
        qmatrix.fromMatrix(imatrix)
       
        q=qmatrix.mult(qaxis)
        omatrix=q.toMatrix()

        norm_x=np.linalg.norm(self.axis_x)
        norm_y=np.linalg.norm(self.axis_y)
        norm_z=np.linalg.norm(self.axis_z)
       
        norm_matrix=np.array([[norm_x,0,0],[0,norm_y,0],[0,0,norm_z]])
        self.matrix=norm_matrix.dot(omatrix)
    
    def Contains(self,p):
        return False
        


      
class C3dPoligon(C3dObject):
     
    def __init__(self,scene,father,color,origin,closed=True,point_list=list(),axis_x=np.array([1,0,0]),axis_y=np.array([0,1,0]),axis_z=np.array([0,0,1])):
        
        C3dObject.__init__(self,scene,father,color,origin,axis_x,axis_y,axis_z)

        self.point_list=point_list

        self.closed=closed

      

    def Render(self):

        if len(self.point_list) > 2 :

            arraypoints=[self.ModeltoWorld(elem[0]*self.axis_x+elem[1]*self.axis_y+self.origin) for elem in self.point_list]

            point_list_=[(x[0],x[1]) for x in arraypoints]

            linethickness=1 if self.isactivereference == False else 2

            pygame.draw.lines(self.screen,self.color,self.closed,point_list_,linethickness)

            C3dObject.Render(self)
        
    def Update(self,delta):

        C3dObject.Update(self,delta)

    def Contains(self,point):

        arraypoints=[self.ModeltoWorld(elem[0]*self.axis_x+elem[1]*self.axis_y+self.origin) for elem in self.point_list]

        polygon = Polygon([(elem[0],elem[1]) for elem in arraypoints])
  
        point = Point(point[0], point[1])
        retval =polygon.contains(point)


class C3dPlane(C3dObject):

    def __init__(self, scene,name, father, color, origin,dim_x,dim_y,dim_z, axis_x,axis_y, axis_z):

        C3dObject.__init__(self, scene,name, father, color, origin, axis_x, axis_y, axis_z)

        self.dim_x=dim_x
        self.dim_y=dim_y
        self.dim_z=dim_z



