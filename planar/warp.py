
import numpy as np

import time
import  json
from os import system, name, path
from utils import *
import cv2 as cv
def layout_horizontal(pw, ph, w, h, pad):
    return (pad, pad)

def layout_vertical(pw, ph, w, h, pad):
    return (pad,h - ph - pad)


def layout_side(pw, ph, w, h, pad):
    return (w - pw - pad, h - ph - pad)

def compose(q,texture,topleft,master_file,width_master,heigth_master,ppp):
    q.acquire()
    if path.exists(master_file):
        master = Image.open(master_file)
    else:
        master = Image.new(texture.mode, (width_master, heigth_master), "white")
    master.paste(master, (0, 0))
    master.paste(texture, (topleft[0],topleft[1]))
    master.show()
    master.save(master_file, dpi=(ppp, ppp))
    master.close()
    q.release()

def warpImage(image, corners, target,side):
    mat=cv.getPerspectiveTransform(corners, target)
    out= cv.warpPerspective(image, mat,(side,side))
    return out

def run(inputfile,inputplane,q):

    #system("cls")
    #prround("*--------------------------------------------------------------------------------------*")
    #prround("*                                                                                      *")
    #prround("*            Anamorphic Box                                                            *")
    #prround("*                                                                                      *")
    #prround("*                                                                                      *")
    #prround("* © Javier de Juan 2020                                                                *")
    #prround("*--------------------------------------------------------------------------------------*")



    # importing the module
    #import json

    # Opening JSON file
    with open(inputfile) as json_file:
        data = json.load(json_file)

        file = data["image_file"]
        ppp = data["ppp"]
        ppcm = ppp / 2.54  # points per centimeters
        ppcm = int(round(ppcm))
        master_file=data['master_file']
        layout=data['layout']
        plane = inputplane
        topleft=np.array(plane['topleft'],dtype=int)
        dir=data['dir']
        #dim=data['dim']

    print("processing planar anamorphose on plane :", plane['name'])
    t0 = time.time()


    #prround("reading the file...", file)

    A = np.array(plane["A"])
    B = np.array(plane["B"])
    C = np.array(plane["C"])
    D = np.array(plane["D"])

    side =int(np.linalg.norm(B-A))
    rect=np.array([A,B,C,D],dtype='float32')
    target = np.array([[0, 0], [side - 1, 0], [side - 1, side - 1], [0, side - 1]], dtype='float32')
    drawing = cv.imread(file)
    out = warpImage(drawing, rect, target, side)
    print("plane->%s side:%s"%(plane['name'],side))
    outputfile=dir+plane['name']+'.jpg'
    cv.imwrite(outputfile, out)

    # topleft=heigth*topleft
    # compose(q,texture,topleft,master_file,width_master,heigth_master,ppp)    #texture.resize((width,heigth))
    # filename=dir+plane['name']+".png"
    # texture.save(filename, dpi=(ppp, ppp))

    t = time.time() - t0  #

    text = "Ending processing planar anamorphose on plane :" + plane["name"]
    text2 = " (Time elapsed:%s (%4.3f s))" % (hh_mm_ss_text(t), t)
    print(text + text2)

