
from multiprocessing import *
import cv2 as cv
import json
import argparse
import time

from warp import *


parser = argparse.ArgumentParser()
parser.add_argument("file", type=str, help="performs anamorphic transformation")
args = parser.parse_args()
inputfile = args.file
with open(inputfile) as json_file:
    data = json.load(json_file)
    planes_dict=data["planes"]

json_file.close()
if __name__ == '__main__':
    jobs=[]
    lock=Lock()

    for plane in planes_dict:
        p=Process(target=run,args=(inputfile,plane,lock))
        p.daemon=True
        p.start()
        jobs.append(p)



    [x.join() for x in jobs]
    [x.terminate() for x in jobs]

   # time.sleep(5)
    print("resizing images..")
    bset=False
    for plane in planes_dict:
        filename=data['dir']+plane['name']+'.jpg'
        print(filename)
        img=cv.imread(filename)
        if bset==False:
            heigth, width, channels = img.shape
            bset=True
        im_r=cv.resize(img, (width,heigth), interpolation = cv.INTER_AREA)
        print("resizing ",filename)
        cv.imwrite(filename,im_r)


