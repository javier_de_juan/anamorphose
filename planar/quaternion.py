import numpy as np

class CQuat():

    def __init__(self):

        self.quat = np.array([0, 0, 0, 0])

    def fromVector(self,vector):
        self.quat=np.array([0,vector[0],vector[1],vector[2]])

    def conj(self):
        aux= np.array([self.quat[0],-self.quat[1],-self.quat[2],-self.quat[3]])
        ret = CQuat()
        ret.quat=aux
        return ret

    def normalize(self,tolerance=0.00001):
        norm=np.linalg.norm(self.quat)
        self.quat=self.quat/norm
              

    def mult(self,q2):

        ret = CQuat()

        w1 = self.quat[0]
        x1 = self.quat[1]
        y1 = self.quat[2]
        z1 = self.quat[3]

        w2 = q2.quat[0]
        x2 = q2.quat[1]
        y2 = q2.quat[2]
        z2 = q2.quat[3]


        w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
        x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
        y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2
        z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2

        ret.quat=np.array([w, x, y, z])
        return ret

    def rotate(self,vect):
        vect_norm=np.linalg.norm(vect)
        vect_aux=np.array(vect)/vect_norm
        p=np.array([0,vect_aux[0],vect_aux[1],vect_aux[2]])
        p=CQuat()
        p.fromVector(vect)
        #p'=q*p*q.conj()
        qaux=self.mult(p)
        p_=qaux.mult(self.conj())

        ret= np.array([p_.quat[1],p_.quat[2],p_.quat[3]])
        ret=ret/np.linalg.norm(ret)
        return ret

    def toMatrix(self):

        qw=self.quat[0]
        qx=self.quat[1]
        qy=self.quat[2]
        qz=self.quat[3]

        return np.array([[1 - 2*qy**2 - 2*qz**2,	2*qx*qy - 2*qz*qw,	    2*qx*qz + 2*qy*qw ],
	                     [2*qx*qy + 2*qz*qw,	1 - 2*qx**2 - 2*qz**2,	2*qy*qz - 2*qx*qw ],
	                     [2*qx*qz - 2*qy*qw,	2*qy*qz + 2*qx*qw,	1 - 2*qx**2 - 2*qy**2 ]])
       
    def fromAxisAngle(self,axis,angle):

        angle=np.radians(angle)
        self.quat=np.array([np.cos(angle/2),np.sin(angle/2)*axis[0],np.sin(angle/2)*axis[1],np.sin(angle/2)*axis[2]])
        self.normalize()
        self.quat=np.array(self.quat)

    def fromMatrix(self,matrix):

        m00,m01,m02 = matrix[0,]
        m10,m11,m12 = matrix[1,]
        m20,m21,m22 = matrix[2,]
       

        tr = m00 + m11 + m22

      
        if tr > 0 :
            S = np.sqrt(tr+1.0) * 2
            qw = 0.25 * S
            qx = (m21 - m12) / S
            qy = (m02 - m20) / S 
            qz = (m10 - m01) / S 
        elif ((m00 > m11)and(m00 > m22)) : 
            S = np.sqrt(1.0 + m00 - m11 - m22) * 2
            qw = (m21 - m12) / S
            qx = 0.25 * S
            qy = (m01 + m10) / S 
            qz = (m02 + m20) / S 
        elif (m11 > m22) : 
            S = np.sqrt(1.0 + m11 - m00 - m22) * 2 
            qw = (m02 - m20) / S
            qx = (m01 + m10) / S 
            qy = 0.25 * S;
            qz = (m12 + m21) / S; 
        else :
            S = np.sqrt(1.0 + m22 - m00 - m11) * 2
            qw = (m10 - m01) / S;
            qx = (m02 + m20) / S;
            qy = (m12 + m21) / S;
            qz = 0.25 * S;
    
        self.quat=np.array([qw,qx,qy,qz])

        return self


