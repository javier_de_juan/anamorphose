from C3DGeom import *
from PIL import Image


class C3DTexture(C3dPlane):
    def __init__(self, scene, name, father, color, origin, ppcm, dim_u, dim_v, axis_x, axis_y, axis_z):
        C3dPlane.__init__(self, scene, name, father, color, origin, dim_u, dim_v, 0, axis_x, axis_y, axis_z)
        self.ppcm = ppcm
        self.dim_u = dim_u
        self.dim_v = dim_v
        self.width = int(ppcm * dim_u)
        self.heigth = int(ppcm * dim_v)
        self.img=None
        self.pixel_map=None

    def GenerateImage(self,mode,create_pixels=True):

        #if create_pixels:
        #    self.pixels = [np.array([u, v, 0],dtype=float) for u in np.arange(0,self.width,1) for v in np.arange(0,self.heigth,1)]
        self.img = Image.new(mode, (self.width, self.heigth), self.color)
        self.pixel_array=np.array(self.img)

    def ModelToWorldFromPixelArray(self, pixelarray):
        #aux=np.copy(pixel)
        #aux[:0] = float(pixel[0]) / self.ppcm
        #aux[:1] = float(pixel[1]) / self.ppcm
        pointarray = ((pixelarray/self.ppcm).dot(self.matrix) + self.origin)
        return pointarray

    def ModelToWorldFromPixel(self, pixel):
        aux=np.copy(pixel)
        aux[0] = float(pixel[0]) / self.ppcm
        aux[1] = float(pixel[1]) / self.ppcm
        point = aux.dot(self.matrix) + self.origin
        return point

    def WorldToModelFromPointArray(self, point_array):
        pixel = ((point_array - self.origin).dot(self.inv_matrix)+np.array([self.dim_u/2,self.dim_v/2,0]))*self.ppcm

        return pixel
    def WorldToModelFromPoint(self, point):
        pixel = (point - self.origin).dot(self.inv_matrix)
        pixel[0] += self.dim_u/2
        pixel[1] += self.dim_v/ 2
        pixel *= self.ppcm
        return int(pixel[0].round(0)),int(pixel[1].round(0))
