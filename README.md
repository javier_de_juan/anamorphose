# Anamorphosis

Studying visual perception through different types of anamorphoses.

Anamorphosis is a distorted projection requiring the viewer to occupy a specific vantage point, use special devices, or both to view a recognizable image. It is used in painting, photography, sculpture and installation, toys, and film special effects. The word is derived from the Greek prefix ana-, meaning "back" or "again", and the word morphe, meaning "shape" or "form" (source: wikipedia)

### planar anamorphose
<div align="center">
![ ALT](planar/images/example2planes3.jpg)

</div>

Building the setup
* Explanation of the geometrical transformation
* setup of your final composition
* Homographies

### running the script
#### calibration script
```python
python calibration.py mycalibration.json
```
```javascript
{
    "file": "calibration",
    "dir":"",
    "phi":0.0,
    "h":115,
    "l": 72,
    "FOV": 38,
    "translation":[0,-12.5,0],
    "homotecy": 1.0,
    "layout":[21.0,29.7], // size of the virtual image (A4)
    "dim": 25,
    "ppp": 600,
	  "line_width":5,
    "line_color":"black",
    "planes":[{
	     "name":"horizontal",
		   "topleft": [1,1],
		   "axis": [0,1],
	     "a": [0,0,0],
	     "b": [0,25,0],
	     "c": [25,25,0],
	     "d":[25,0,0]
	     },
		 {
	     "name":"vertical",
			 "axis": [1,2],
		   "topleft": [1,0],
		   "a": [0,0,25],
		   "b": [0,25,25],
		   "c":[0,25,0],
	     "d": [0,0,0]
	     },
    	{
	     "name":"side",
			 "axis": [0,2],
			 "topleft": [0,0], 
		   "a":[25,0,25],
	     "b": [0,0,25],
	     "c": [0,0,0],
	     "d": [25,0,0]
	     }
	],
	"cubes": [
//		{"name": "cube1",
//			"faces": [ {"name":"top", "a":[0,0,10], "b":[0,10,10], "c":[10,10,10], "d":[10,0,10]},
//				        {"name":"right", "a":[10,10,10], "b":[0,10,10], "c":[0,10,0], "d":[10,10,0]},
//						{"name":"left", "a":[10,0,10], "b":[10,10,10], "c":[10,10,0], "d":[10,0,0]}]
//		}

	]

}
this script will produce the following calibration image as well as the template script for acheiving the requested anamorphose:
```
<div align="center">
![ ALT](planar/images/calibration-Phi0-Theta32-f_4.6-R18.2.png)
</div>

and the template script:

```javascript
{  
    "image_file": "composition-Phi1-Theta32-f 4.6-R18.2.jpg",
    "master_file": "master.png",
    "dir": "",
    "drawing_heigth_cm": 29.7,
    "drawing_width_cm" : 21.0,
    "ppp": 150,
    "layout": [21.0,29.7],
    "planes": [
        {
            "name": "horizontal",
            "axis": [0,1
            ],
            "topleft": [1,1],
            "a": [0,0],
            "b": [0,10],
            "c": [10,10],
            "d": [10,0],
            "A": [620,876],
            "B": [976,1066],
            "C": [620,1296],
            "D": [263,1066]
        },
        {
            "name": "vertical",
            "axis": [1,2],
            "topleft": [1,0],
            "a": [0,10],
            "b": [10,10],
            "c": [10,0],
            "d": [0,0],
            "A": [620,454],
            "B": [1010,616],
            "C": [976,1066],
            "D": [620,876]
        }
       
    ]
}
```
now with the help of an image editor tool we insert the desired anamorphed image into the calibration image:

<div align="center">
![ ALT](planar/calibration-Phi0-Theta32-f_4.6-R18.2-modelo-72.png)
</div>

This is the image the visual percpetion system should represent in our heads.
Now we are ready to run the geometrical transformation (utilizing *homographies*)
```python
python anamorph.py <script.json>
```
anamorph script is going to build the required projections on the requested planes, in our case, it will produce horizontal and vertical files:
<div align="center">
![ ALT](planar/images/vertical2.jpg)
</div>
<div align="center">
![ ALT](planar/images/horizontal2.jpg)
</div>

The last step is then the compose the final image, adding the desired background:
<div align="center">
![ ALT](planar/images/master-chica.png)
</div>
